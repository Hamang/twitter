# Removing ‘@names’

def remove_pattern(text, pattern_regex, c=''):
    r = re.findall(pattern_regex, text)
    for i in r:
        text = re.sub(i, c, text)

    return text

# Removing Stop Words

from nltk.corpus import stopwords

nltk.download('stopwords')
stopwords_set = set(stopwords.words("english"))
cleaned_tweets = []

for index, row in tweets_df.iterrows():
    # filerting out all the stopwords
    words_without_stopwords = [word for word in row.absolute_tidy_tweets.split() if not word in stopwords_set]

    # finally creating tweets list of tuples containing stopwords(list) and sentimentType
    cleaned_tweets.append(' '.join(words_without_stopwords))

tweets_df['absolute_tidy_tweets'] = cleaned_tweets

# Tokenization and lemmatization

from nltk.stem import WordNetLemmatizer# Tokenization
tokenized_tweet = tweets_df['absolute_tidy_tweets'].apply(lambda x: x.split())
# Finding Lemma for each word
word_lemmatizer = WordNetLemmatizer()
tokenized_tweet = tokenized_tweet.apply(lambda x: [word_lemmatizer.lemmatize(i) for i in x])
#joining words into sentences (from where they came from)
for i, tokens in enumerate(tokenized_tweet):
    tokenized_tweet[i] = ' '.join(tokens)

tweets_df['absolute_tidy_tweets'] = tokenized_tweet